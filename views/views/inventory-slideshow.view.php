<?php

if ($this->car_list != null) {
$carsToDisplay = [];
foreach($this->car_list as $car)
{
    if ($car->ImageUrls[0]) $carsToDisplay[] = $car;
}
shuffle($carsToDisplay);
}
?>

<div class="fullslide owl-carousel" >
        <?php
        foreach($carsToDisplay as $key => $car)
        {            ?>            <div class="item">            <?php
            echo '<a href="'.$this->generate_detail_url($car).'"><img src="'.$car->ImageUrls[0].'" id="slideImg'.$key.'"  /></a>';            ?>             <div class="ymcarsoal-style"><?php echo $car->Year;?> <?php echo $car->Make;?> <?php  echo $car->Model; ?> <br/> $<?php  echo $car->Price; ?></div>               </div>            <?php
        }
        ?></div>