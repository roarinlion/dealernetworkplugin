<?php

$terms_list = array();
$display_list = array();

foreach ($this->car_list as $car) {
	array_push($terms_list, $car->Category);
	array_push($terms_list, $car->Year);
	array_push($terms_list, $car->Make);
	array_push($terms_list, $car->Model);
}

foreach($terms_list as $term)
{
	if (!in_array($term, $display_list)) array_push($display_list, $term);
}
?>

<script>
  jQuery(function()
  {
    var availableTags =
	[
  <?php
	$terms_count = count($display_list);
	for ($i = 0; $i < $terms_count - 2; $i++)
	{
		echo '"'.$display_list[$i].'",';
		echo "\n";
	}
	echo '"'.$display_list[$terms_count - 1].'"';
	?>
    ];
	
    jQuery( "#q" ).autocomplete({
      source: availableTags
    });
  });
 </script>
<center>  
<form id="autocomplete_search_form" method='get' action='../inventory/'>

<input id="q" name="q" placeholder=" I'm Looking for a... " style="display: inline-block; width: 60%; height: 40px; border-collapse: collapse; border: 1px solid grey; border-radius: 5px; margin: 10px; padding: 5px;" />

<input type="submit" value="Search" style="display: inline-block; width: 30%;">
</form>
</center>